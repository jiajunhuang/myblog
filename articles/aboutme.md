# 关于我

> 会当凌绝顶，一览众山小。

我是 `Jiajun Huang`，2016年毕业于软件工程专业。在分布式系统和高并发处理方面有一些粗浅的经验。
目前主攻Web后端和中间件，工作上用的最多的是 `Python` 和 `Golang` ，平时也玩玩 `Haskell` 和 `C`。

个人认为把复杂的东西用平易近人的方式描述出来是一种很重要的能力，并且一直都在锻炼这种能力，下面是我写的一些
科普性的系列文章：

- [从零构建TCP](https://jiajunhuang.com/articles/2017_08_12-tcp_ip.md.html)
- [一步一步学习Haskell](https://jiajunhuang.com/articles/2017_09_11-learn_you_a_haskell_part_1.md.html)
- [Web开发简介](https://jiajunhuang.com/articles/2017_10_19-web_dev_series.md.html)
- [密码技术简明教程](https://jiajunhuang.com/articles/2019_05_12-crypto.md.html)
- [Golang(Go 语言)简明教程](https://jiajunhuang.com/tutorial/golang/index.md)

更多的文章请访问 [主页](https://jiajunhuang.com/)。 **请不要找我写书和录课，我没有此类计划**。

## 订阅博客

- [订阅RSS](https://jiajunhuang.com/rss)
- [订阅Telegram Channel](https://t.me/jiajunhuangcom)

## 我的联系方式:

- 邮件: `gansteed(at, you know)gmail.com`
- Github: https://github.com/jiajunhuang
- Telegram: https://t.me/jiajunhuang

[个人作品请看这里](https://jiajunhuang.com/projects)
